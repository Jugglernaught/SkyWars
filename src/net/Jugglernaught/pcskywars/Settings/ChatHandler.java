package net.Jugglernaught.pcskywars.Settings;

import net.Jugglernaught.pcskywars.SkyWarsMain;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.connorlinfoot.actionbarapi.ActionBarAPI;
import com.connorlinfoot.titleapi.TitleAPI;

/**
 * ChatHandler
 *
 * @author Jugglernaught
 *
 * @version 10/18/15
 */

public class ChatHandler {
    private static String prefix = ChatColor.AQUA + "[PCSW] " + ChatColor.RESET;

    public static void broadcastToPlayers(SkyWarsMain pl, String message) {
    	for (Player p : Bukkit.getOnlinePlayers()) {
    		if (p == null) continue;
    		TitleAPI.sendTitle(p, 5, 30, 5, "", message);
    	}
    }

    public static void broadcastToPlayers(SkyWarsMain pl, String message, Player[] players) {
        for (Player p : players) {
        	if (p == null) continue;
        	TitleAPI.sendTitle(p, 5, 30, 5, "", message);
        }
    }

    public static void sendMessage(Player p, String message) {
    	if (p == null) return;
        p.sendMessage(prefix + ChatColor.GREEN + message);
    }
    
    public static void sendMiniMessage(SkyWarsMain pl, String message, Player[] players) {
    	for (Player p : players) {
        	if (p == null) continue;
        	ActionBarAPI.sendActionBar(p, message);
        }
    }
}
