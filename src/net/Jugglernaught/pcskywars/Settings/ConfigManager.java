package net.Jugglernaught.pcskywars.Settings;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import net.Jugglernaught.pcskywars.SkyWarsMain;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.PluginDescriptionFile;

/**
 * ConfigManager
 *
 * @author Jugglernaught
 *
 * @version 10/21/15
 */

public class ConfigManager {
	SkyWarsMain pl = SkyWarsMain.pl;
	
    FileConfiguration config;
    File cfile;

    FileConfiguration arenas;
    File dfile;

    FileConfiguration kits;
    File kfile;

	public void setup(SkyWarsMain p) {		
        cfile = new File(p.getDataFolder(), "config.yml");

        if (!p.getDataFolder().exists()) {
            p.getDataFolder().mkdir();
        }
        
        if (!cfile.exists())
        	copy(p.getResource("baseconfig.yml"), cfile);
        
        config = YamlConfiguration.loadConfiguration(cfile);
                
        dfile = new File(p.getDataFolder(), "arenas.yml");

        if (!dfile.exists()) {
            try {
                dfile.createNewFile();
            }
            catch (IOException e) {
                Bukkit.getServer().getLogger().severe(ChatColor.RED + "Could not create arenas.yml!");
            }
        }
        
        arenas = YamlConfiguration.loadConfiguration(dfile);

        kfile = new File(p.getDataFolder(), "kits.yml");
/*
        if (!kfile.exists()) {
            try {
                kfile.createNewFile();
            }
            catch (IOException e) {
                Bukkit.getServer().getLogger().severe(ChatColor.RED + "Could not create kits.yml!");
            }
        }
        
        copy(p.getResource("kits.yml"), kfile);

        kits = YamlConfiguration.loadConfiguration(kfile);
*/    }

    public FileConfiguration getArenas() {
        return arenas;
    }

    public void saveArenas() {
        try {
            arenas.save(dfile);
        }
        catch (IOException e) {
            Bukkit.getServer().getLogger().severe(ChatColor.RED + "Could not save arenas.yml!");
        }
    }

    public void reloadArenas() {
        arenas = YamlConfiguration.loadConfiguration(dfile);
    }

    public FileConfiguration getConfig() {
        return config;
    }

    public void saveConfig() {
        try {
            config.save(cfile);
        }
        catch (IOException e) {
            Bukkit.getServer().getLogger().severe(ChatColor.RED + "Could not save config.yml!");
        }
    }

    public void reloadConfig() {
        config = YamlConfiguration.loadConfiguration(cfile);
    }

    public PluginDescriptionFile getDesc() {
        return pl.getDescription();
    }

    public FileConfiguration getKits() {
        return kits;
    }

    public void saveKits() {
        try {
            config.save(kfile);
        }
        catch (IOException e) {
            Bukkit.getServer().getLogger().severe(ChatColor.RED + "Could not save kits.yml!");
        }
    }

    public void reloadKits() {
        kits = YamlConfiguration.loadConfiguration(kfile);
    }
    
    public void copy(InputStream in, File f) {
    	try {
			FileOutputStream fos = new FileOutputStream(f);
			byte[] buffer = new byte[1024];
			int len;
			
			while ((len = in.read(buffer)) > 0) {
				fos.write(buffer, 0, len);
			}
			
			fos.close();
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
}
