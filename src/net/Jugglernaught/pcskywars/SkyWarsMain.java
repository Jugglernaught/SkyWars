package net.Jugglernaught.pcskywars;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import net.Jugglernaught.pcskywars.Settings.ChatHandler;
import net.Jugglernaught.pcskywars.Settings.ConfigManager;
import net.Jugglernaught.pcskywars.Settings.SQLManager;
import net.Jugglernaught.pcskywars.arenas.Arena;
import net.Jugglernaught.pcskywars.game.Game;
import net.Jugglernaught.pcskywars.game.GameState;
import net.Jugglernaught.pcskywars.game.SkyWar;
import net.Jugglernaught.pcskywars.listeners.Damage;
import net.Jugglernaught.pcskywars.listeners.Death;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * net.Jugglernaught.pcskywars.SkyWarsMain
 *
 * @author Jugglernaught
 *
 * @version 10/18/15
 */

public class SkyWarsMain extends JavaPlugin {
    public static SkyWarsMain pl;

    private final Logger log = Logger.getLogger("Minecraft");
    private final Death death = new Death();
    private final Damage damage = new Damage();

    private ConfigManager cm = new ConfigManager();

    public void onEnable() {
        SkyWarsMain.pl = this;
        
        if (SQLManager.openConnection()) {
        	log.info("[PSW] SQL Connection has been established!");
        }

        log.info("[PSW] Skywars plugin has been fully enabled!");
        getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");

        PluginManager pm = this.getServer().getPluginManager();
        pm.registerEvents(death, this);
        pm.registerEvents(damage, this);

        cm.setup(pl);
        Game.setup(cm.getArenas());
    }

    public void onDisable() {
        SQLManager.closeConnection();
    }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("You must be a player to use this plugin's commands!");
        }

        Player p = (Player)sender;

        if (label.equalsIgnoreCase("psw")) {
            if (args.length > 0) {
            	if (args[0].equalsIgnoreCase("list")) {
            		if (!p.hasPermission("psw.list")) {
            			return false;
            		}
            		if (args.length == 1) {
            			String list = "Arenas: \n";
            			for (Game game : Game.getCurrentGames()) {
            				if (game == null) continue;
            				list = list + game.getSkywar().getName() + "\n";
            			}
            			ChatHandler.sendMessage(p, list);
            		}
            	}
            	else if (args[0].equalsIgnoreCase("join")) {
            		if (!p.hasPermission("psw.basic"))
            			return false;
            		if (args.length == 2 && Game.getCurrentGame(args[1]) != null) {
                        Game g = Game.getCurrentGame(args[1]);
                        
                        if (g.getSkywar().getArena().getLobby() == null) {
                        	ChatHandler.sendMessage(p, "This arena doesn't have a lobby! " +
                        			"You cannot join.");
                        	return true;
                        }
                        if (g.getSkywar().getArena().getSpawns() == null || 
                        		g.getSkywar().getArena().getSpawns()[0] == null) {
                        	ChatHandler.sendMessage(p, "This arena doesn't have any spawns! " +
                        			"You cannot join.");
                        	return true;
                        }
                        
                        for (Game game : Game.getCurrentGames()) {
                        	if (game == null) continue;
                        	if (game.getSkywar().containsPlayer(p)) {
                        		ChatHandler.sendMessage(p, "You cannot join a game if you are still in one!");
                        		return true;
                        	}
                        }
                        
                        if (g.getSkywar().addPlayer(p))
                            ChatHandler.sendMessage(p, "You have been added to the arena: " + args[1]);
                        
                        return true;
                    }
                    ChatHandler.sendMessage(p, "Arena could not be found or invalid number of arguments!");
                    return true;
            	}
            	if (args[0].equalsIgnoreCase("leave")) {
            		if (!p.hasPermission("psw.basic"))
            			return false;
            		if (args.length == 1) {
                        for (Game game : Game.getCurrentGames()) {
                        	if (game == null) continue;
                        	if (!game.getSkywar().containsPlayer(p)) {
                        		ChatHandler.sendMessage(p, "You cannot leave a game if you aren't in one!");
                        		return true;
                        	}
                        	if (game.getSkywar().removePlayer(p)) {
                        		p.teleport(p.getWorld().getSpawnLocation());
                        		if (game.getSkywar().getGameState().equals(GameState.ACTIVE))
                        			game.killPlayer(p);
                        		ChatHandler.sendMessage(p, "You have successfully left your game.");
                        		return true;
                        	}
                        }
                    }
                    ChatHandler.sendMessage(p, "Arena could not be found or invalid number of arguments!");
                    return true;
            	}
                if (args[0].equalsIgnoreCase("item")) {
                    if (!p.hasPermission("psw.admin"))
                        return false;
                    ChatHandler.sendMessage(p, p.getItemInHand().toString());
                }
                if (args[0].equalsIgnoreCase("create")) {
                    if (!p.hasPermission("psw.admin"))
                        return false;
                    Arena a;
                    if (args.length == 2) {
                        if (!cm.getArenas().getKeys(false).contains(args[1])) {
                            a = new Arena(args[1], getConfig().getInt("default-min-players"),
                                    getConfig().getInt("default-max-players"), null, p.getLocation(), null, null);
                            ChatHandler.sendMessage(p, "Please use command again to set 2nd location of arena");
                            a.toConfig(cm.getArenas());
                            cm.saveArenas();
                            return true;
                        }
                        if (cm.getArenas().getString(args[1] + ".bound2") != null) {
                            ChatHandler.sendMessage(p, "Arena already exists!");
                            return true;
                        }
                        Location l = (Location) cm.getArenas().get(args[1] + ".bound1");

                        a = new Arena(args[1], getConfig().getInt("default-min-players"),
                                getConfig().getInt("default-max-players"), null, l, p.getLocation(), null);
                        a.toConfig(cm.getArenas());

                        ChatHandler.sendMessage(p, "Successfully created arena: " + args[1]);

                        Game.addToGames(new Game(new SkyWar(a, GameState.WAITING, new Player[a.getMaxPlayers()])));

                        cm.saveArenas();
                        return true;
                    }
                    ChatHandler.sendMessage(p, "Invalid number of arguments!");
                    return true;
                }
                if (args[0].equalsIgnoreCase("remove")) {
                    if (!p.hasPermission("psw.admin"))
                        return false;
                    if (args.length == 2 && cm.getArenas().getKeys(false).contains(args[1])) {
                        cm.getArenas().set(args[1], null);
                        ChatHandler.sendMessage(p, "Arena successfully deleted!");
                        Game.setup(cm.getArenas());
                        return true;
                    }
                    ChatHandler.sendMessage(p, "Arena could not be found or invalid number of arguments!");
                    return true;
                }
                if (args[0].equalsIgnoreCase("setlobby")) {
                    if (!p.hasPermission("psw.admin"))
                        return false;
                    if (args.length == 2 && cm.getArenas().getKeys(false).contains(args[1])) {
                        cm.getArenas().set(args[1] + ".lobby", p.getLocation());
                        ChatHandler.sendMessage(p, "Lobby successfully set!");
                        for (Game g : Game.getCurrentGames()) {
                        	if (g == null) continue;
                            if (g.getSkywar().getArena().getName().equalsIgnoreCase(args[1])) {
                                g.getSkywar().getArena().reloadArena(cm.getArenas());
                            }
                        }
                        
                        cm.saveArenas();
                        return true;
                    }
                    ChatHandler.sendMessage(p, "Arena could not be found or invalid number of arguments!");
                    return true;
                }
                if (args[0].equalsIgnoreCase("setmaxplayers")) {
                    if (!p.hasPermission("psw.admin"))
                        return false;
                    if (args.length == 3 && cm.getArenas().getKeys(false).contains(args[1])) {
                        int i;
                        try {
                            i = Integer.parseInt(args[2]);
                        } catch (Exception e) {
                            ChatHandler.sendMessage(p, "Invalid number!");
                            return true;
                        }
                        cm.getArenas().set(args[1] + ".maxplayers", i);
                        cm.saveArenas();
                        ChatHandler.sendMessage(p, "Max players successfully set!");
                        for (Game g : Game.getCurrentGames()) {
                        	if (g == null) continue;
                            if (g.getSkywar().getArena().getName().equalsIgnoreCase(args[1])) {
                                g.getSkywar().getArena().reloadArena(cm.getArenas());
                            }
                        }
                        return true;
                    }
                    ChatHandler.sendMessage(p, "Arena could not be found or invalid number of arguments!");
                    return true;
                }
                if (args[0].equalsIgnoreCase("setminplayers")) {
                    if (!p.hasPermission("psw.admin"))
                        return false;
                    if (args.length == 3 && cm.getArenas().getKeys(false).contains(args[1])) {
                        int i;
                        try {
                            i = Integer.parseInt(args[2]);
                        } catch (Exception e) {
                            ChatHandler.sendMessage(p, "Invalid number!");
                            return true;
                        }
                        cm.getArenas().set(args[1] + ".minplayers", i);
                        cm.saveArenas();
                        ChatHandler.sendMessage(p, "Min players successfully set!");
                        for (Game g : Game.getCurrentGames()) {
                        	if (g == null) continue;
                            if (g.getSkywar().getArena().getName().equalsIgnoreCase(args[1])) {
                                g.getSkywar().getArena().reloadArena(cm.getArenas());
                            }
                        }
                        return true;
                    }
                    ChatHandler.sendMessage(p, "Arena could not be found or invalid number of arguments!");
                    return true;
                }
                if (args[0].equalsIgnoreCase("addspawn")) {
                    if (!p.hasPermission("psw.admin"))
                        return false;
                    if (args.length == 2 && cm.getArenas().getKeys(false).contains(args[1])) {
                    	List<String> a;
                    	if (cm.getArenas().getStringList(args[1] + ".spawns") != null) {
                    		a = cm.getArenas().getStringList(args[1] + ".spawns");
                    	}
                    	else {
                    		a = new ArrayList<String>();
                    	}
                    	a.add(Game.getCurrentGame(args[1]).getSkywar().getArena().locToString(p.getLocation()));
                    	cm.getArenas().set(args[1] + ".spawns", a);

                        ChatHandler.sendMessage(p, "Successfully added a spawn point!");
                        for (Game g : Game.getCurrentGames()) {
                        	if (g == null) continue;
                            if (g.getSkywar().getArena().getName().equalsIgnoreCase(args[1])) {
                                g.getSkywar().getArena().reloadArena(cm.getArenas());
                            }
                        }
                        cm.saveArenas();
                        return true;
                    }
                    ChatHandler.sendMessage(p, "Arena could not be found or invalid number of arguments!");
                    return true;
                }
                if (args[0].equalsIgnoreCase("removespawn")) {
                    if (!p.hasPermission("psw.admin"))
                        return false;
                    if (args.length == 2 && cm.getArenas().getKeys(false).contains(args[1])) {
                        Set<String> a = cm.getArenas().getConfigurationSection(args[1] + ".spawns").getKeys(true);

                        a.remove(a.toArray()[a.size() - 1]);

                        cm.getArenas().set(args[1] + ".spawns", a);

                        ChatHandler.sendMessage(p, "Successfully removed  the most recently added spawn point!");
                        for (Game g : Game.getCurrentGames()) {
                        	if (g == null) continue;
                            if (g.getSkywar().getArena().getName().equalsIgnoreCase(args[1])) {
                                g.getSkywar().getArena().reloadArena(cm.getArenas());
                            }
                        }
                        cm.saveArenas();
                        return true;
                    }
                    ChatHandler.sendMessage(p, "Arena could not be found or invalid number of arguments!");
                    return true;
                }
                if (args[0].equalsIgnoreCase("start")) {
                    if (!p.hasPermission("psw.admin"))
                        return false;
                    if (args.length == 2 && cm.getArenas().getKeys(false).contains(args[1])) {
                        for (Game g : Game.getCurrentGames()) {
                        	if (g == null) continue;
                            if (g.getSkywar().getArena().getName().equals(args[1])) {
                                g.getSkywar().override();
                                ChatHandler.sendMessage(p, "Game successfully started!");
                                return true;
                            }
                        }
                        ChatHandler.sendMessage(p, "Could not find active game by that name: " + args[1]);
                        return true;
                    }
                    ChatHandler.sendMessage(p, "Game could not be found or invalid number of arguments!");
                    return true;
                }
                if (args[0].equalsIgnoreCase("stop")) {
                    if (!p.hasPermission("psw.admin"))
                        return false;
                    if (args.length == 2 && cm.getArenas().getKeys(false).contains(args[1])) {
                        for (Game g : Game.getCurrentGames()) {
                        	if (g == null) continue;
                            if (g.getSkywar().getArena().getName().equals(args[1])) {
                                g.stopGame();
                                ChatHandler.sendMessage(p, "Game successfully stopped!");
                                return true;
                            }
                        }
                        ChatHandler.sendMessage(p, "Could not find active game by that name: " + args[1]);
                        return true;
                    }
                    ChatHandler.sendMessage(p, "Game could not be found or invalid number of arguments!");
                    return true;
                }
                if (args[0].equalsIgnoreCase("reload")) {
                    if (!p.hasPermission("psw.admin"))
                        return false;
                    cm.reloadArenas();
                    cm.reloadConfig();
                    cm.reloadKits();
                    Game.setup(cm.getArenas());
                    ChatHandler.sendMessage(p, "Pineapplecraft SkyWars has been reloaded!");
                }
                if (args[0].equalsIgnoreCase("help")) {
                    if (p.hasPermission("psw.admin")) {
                        p.sendMessage( ChatColor.RED +
                                "     =================\n" +
                                        "     |  Help Menu    |\n" +
                                        "     ====================\n" +
                                        "/psw help  -- returns this help menu\n" +
                                        "/psw list  -- lists the available games\n" +
                                        "/psw create [arena name]\n" +
                                        " - Creates an arena with the given name\n" +
                                        "/psw remove [arena name]\n" +
                                        " - removes the arena with the given name\n" +
                                        "/psw setlobby [arena name]\n" +
                                        " - sets the lobby for the given arena\n" +
                                        "/psw setminplayers [arena name] [number]\n" +
                                        " - sets the minimum players for the arena\n" +
                                        "/psw setmaxplayers [arena name] [number]\n" +
                                        " - sets the maximum players for the arena\n" +
                                        "/psw addspawn [arena name]\n" +
                                        " - adds a spawn to the arena *recommended 8 spawns\n" +
                                        "/psw removespawn [arena name]\n" +
                                        " - remove the most recent spawn you added\n" +
                                        "/psw start [arena name]\n" +
                                        " - forces the game with the given arena to start\n" +
                                        "/psw stop [arena name]\n" +
                                        " - forces the game with the given arena to stop\n" +
                                        "/psw reload" +
                                        " - reloads the plugin"
                        );
                        return true;
                    }
                    if (p.hasPermission("psw.basic")) {
                        p.sendMessage( ChatColor.RED +
                                "     =========================\n" +
                                        "     |     Help Menu         |\n" +
                                        "     =========================\n" +
                                        "/psw help  -- returns this help menu");
                    }
                }
            }
            else {
                    if (p.hasPermission("psw.admin")) {
                        p.sendMessage( ChatColor.RED +
                                		"     ============\n" +
                                        "     |  Help Menu    |\n" +
                                        "     ============\n" +
                                        "/psw help  -- returns this help menu\n" +
                                        "/psw list  -- lists all the available games\n" +
                                        "/psw create [arena name]\n" +
                                        " - Creates an arena with the given name\n" +
                                        "/psw remove [arena name]\n" +
                                        " - removes the arena with the given name\n" +
                                        "/psw setlobby [arena name]\n" +
                                        " - sets the lobby for the given arena\n" +
                                        "/psw setminplayers [arena name] [number]\n" +
                                        " - sets the minimum players for the arena\n" +
                                        "/psw setmaxplayers [arena name] [number]\n" +
                                        " - sets the maximum players for the arena\n" +
                                        "/psw addspawn [arena name]\n" +
                                        " - adds a spawn to the arena *recommended 8 spawns\n" +
                                        "/psw removespawn [arena name]\n" +
                                        " - remove the most recent spawn you added\n" +
                                        "/psw start [arena name]\n" +
                                        " - forces the game with the given arena to start\n" +
                                        "/psw stop [arena name]\n" +
                                        " - forces the game with the given arena to stop\n" +
                                        "/psw reload" +
                                        " - reloads the plugin"
                        );
                        return true;
                    }
                    if (p.hasPermission("psw.basic")) {
                        p.sendMessage( ChatColor.RED +
                                		"     ===============" +
                                        "     |     Help Menu      |" +
                                        "     ===============" +
                                        "/psw help  -- returns this help menu");
                }
            }
        }

        return false;
    }

    public void sendToServer(Player player, String targetServer) {
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(b);

        try {
            out.writeUTF("Connect");
            out.writeUTF(targetServer);
        } catch (Exception e) {
            e.printStackTrace();
        }

        player.sendPluginMessage(this, "BungeeCord", b.toByteArray());
    }
}