package net.Jugglernaught.pcskywars.arenas;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;

/**
 * Arena
 *
 * @author Jugglernaught
 *
 * @version 10/18/15
 */

public class Arena {
    private String name;
    private int minPlayers;
    private int maxPlayers;
    private Location bound1;
    private Location bound2;
    private Location lobby;
    private Location[] spawns;

    public Arena(String name, int minPlayers, int maxPlayers, Location[] spawns, Location bound1,
                 Location bound2, Location lobby) {
        this.name = name;
        this.minPlayers = minPlayers;
        this.minPlayers = maxPlayers;
        this.spawns = spawns;
        this.bound1 = bound1;
        this.bound2 = bound2;
        this.lobby = lobby;
    }

    public String getName() {
        return name;
    }

    public int getMinPlayers() {
        return minPlayers;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public Location[] getSpawns() {
        return spawns;
    }

    public Location getBound1() {
        return bound1;
    }

    public Location getBound2() {
        return bound2;
    }

    public Location getLobby() {
        return lobby;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMinPlayers(int min) {
        this.minPlayers = min;
    }

    public void setMaxPlayers(int max) {
        this.maxPlayers = max;
    }

    public void setSpawns(Location[] spawns) {
        this.spawns = spawns;
    }

    public boolean addSpawn(Location spawn) {
        for (int i = 0; i < spawns.length; i++) {
            if (spawns[i] == null) {
                spawns[i] = spawn;
                return true;
            }
        }

        spawns = resizeSpawns();

        for (int i = 0; i < spawns.length; i++) {
            if (spawns[i] == null) {
                spawns[i] = spawn;
                return true;
            }
        }

        return false;
    }

    public boolean removeSpawn (Location spawn) {
        int temp = -1;
        for (int i = 0; i < spawns.length; i++) {
            if (spawns[i].equals(spawn)) {
                spawns[i] = null;
                temp = i;
            }
        }

        if (temp == -1)
            return false;

        for (int i = temp; i < spawns.length; i++) {
            if (i + 1 > spawns.length)
                break;
            spawns[i] = spawns[i + 1];
        }

        return true;
    }

    public void setBound1(Location bound1) {
        this.bound1 = bound1;
    }

    public void setBound2(Location bound2) {
        this.bound2 = bound2;
    }

    public void setLobby(Location lobby) {
        this.lobby = lobby;
    }

    public void toConfig(FileConfiguration f) {
        f.set(this.name + ".minplayers", this.minPlayers);
        f.set(this.name + ".maxplayers", this.maxPlayers);
        f.set(this.name + ".spawns", null);
        if (spawns != null && spawns[0] != null) {
    		ArrayList<String> a = new ArrayList<String>();
        	for (Location l : spawns) {
        		a.add(locToString(l));
        	}
        		f.set(this.name + "." + "spawns", this.spawns);
        }
        if (bound1 != null)
            f.set(this.name + "." + "bound1", this.bound1);
        if (bound2 != null)
            f.set(this.name + "." + "bound2", this.bound2);
        if (lobby != null)
            f.set(this.name + "." + "lobby", this.lobby);

    }

    public void reloadArena(FileConfiguration f) {
        setBound1((Location) f.get(name + ".bound1"));
        setBound2((Location) f.get(name + ".bound2"));
        setLobby((Location) f.get(name + ".lobby"));
        setMaxPlayers(f.getInt(name + ".maxplayers"));
        setMinPlayers(f.getInt(name + ".minplayers"));
        if (f.getStringList(name + ".spawns") != null) 
        	if (f.getStringList(name + ".spawns").size() > 0) {
        		Location[] spawns = new Location[f.getStringList(name + ".spawns").size()];
        		for (int i = 0; i < f.getStringList(name + ".spawns").size(); i++) {
        			spawns[i] = stringToLoc(f.getStringList(name + ".spawns").toArray()[i].toString());
            	}
        		
        		setSpawns(spawns);
        	}
    }
    
    public String locToString(Location l) {
    	String s = "";
    	s = l.getWorld().getName() + "," + l.getX() + "," + l.getY() +  "," +
    			l.getZ() + "," + l.getPitch() + "," + l.getYaw();
    	return s;
    }
    
    public Location stringToLoc(String s) {
    	Location l;
    		String[] a = s.split(",");
    		l = new Location(Bukkit.getWorld(a[0]), 
    				Double.parseDouble(a[1]), 
    				Double.parseDouble(a[2]),
    				Double.parseDouble(a[3]), 
    				Float.parseFloat(a[4]), 
    				Float.parseFloat(a[5]));
    	return l;
    }

    private Location[] resizeSpawns() {
        Location[] newSpawns = new Location[2 + spawns.length];

        for (int i = 0; i < spawns.length; i++) {
            newSpawns[i] = spawns[i];
        }

        return newSpawns;
    }
}
