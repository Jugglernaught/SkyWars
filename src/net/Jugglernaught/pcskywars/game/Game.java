package net.Jugglernaught.pcskywars.game;

import java.util.Random;

import net.Jugglernaught.pcskywars.SkyWarsMain;
import net.Jugglernaught.pcskywars.Settings.ChatHandler;
import net.Jugglernaught.pcskywars.Settings.SQLManager;
import net.Jugglernaught.pcskywars.arenas.Arena;
import net.Jugglernaught.pcskywars.threads.Timer;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

/**
 * Game
 *
 * @author Jugglernaught
 *
 * @version 10/18/15
 */

public class Game {
    private SkyWar skywar;
    private static Game[] currentGames = new Game[10];
    private boolean isFinished;
    private Player[] stillAlive;
    private Timer timer;

    public Game(SkyWar skywar) {
        this.skywar = skywar;
        isFinished = false;
        timer = new Timer(SkyWarsMain.pl.getConfig().getInt("time-before-game-starts"), this, SkyWarsMain.pl);
        addToGames(this);
    }

    public SkyWar getSkywar() {
        return skywar;
    }

    public void startGame() {
        registerPlayers();
        
        stillAlive = skywar.getPlayers();
        
        for (int i = 0; i < skywar.getPlayers().length; i++) {
            TeamColor color;
            Random r = new Random();
            Team t = new Team(null, null);
            Player[] p = {skywar.getPlayers()[i]};
                
             for (int c = 0; c < skywar.getPlayers().length / 2 + skywar.getPlayers().length % 2; c++) {
                color = TeamColor.values()[r.nextInt(8)];
                for (int temp = 0; temp < 8; temp++) {
                	if (skywar.getTeams()[temp] == null) {
                		t = new Team(p, color);
                	}
                	else if(skywar.getTeams()[temp].getTeamColor().equals(color) &&
                			temp + 1 < 8) {
                		color = TeamColor.getColors()[temp + 1];
                		t = new Team(p, color);
                	}
                	else if (skywar.getTeams()[temp].getTeamColor().equals(color) && temp + 1 >= 8) {
                		color = TeamColor.getColors()[i % 8];
                		t = new Team(p, color);
                	}
                	else if (!skywar.getTeams()[temp].getTeamColor().equals(color)) {
                		t = new Team(p, color);
                	}
                	else 
                		break;
                }
                	
                if (t != null) {
                	skywar.updateTeam(t);
                	skywar.getPlayers()[i].sendMessage(t.getTeamColor().getChatColor() + 
                		"You are now " + t.getTeamColor().getName() + " team!");
                }
            }
        }

        Location[] spawns = skywar.getArena().getSpawns();
        
        for (int a = 0; a < skywar.getTeams().length; a++) {
            Team t = skywar.getTeams()[a];
            if (t == null) {
            	ChatHandler.broadcastToPlayers(SkyWarsMain.pl, "Null Team");
            	continue;
            }
            
            int spawnNumber = a;
            
        	for (int i = 1; i < 30; i++) {
            	if (spawnNumber >= spawns.length) {
            		spawnNumber = a - i*a;
            	}
            }
            
            for (int i = 0; i < t.getPlayers().length; i++) {
            	Player p = t.getPlayers()[i];
            	if (p == null) {
            		ChatHandler.broadcastToPlayers(SkyWarsMain.pl, "Null Player");
            		continue;
            	}
            		p.setGameMode(GameMode.SURVIVAL);
            		p.teleport(skywar.getArena().getSpawns()[spawnNumber]);
            }
        }

        SQLManager.registerNewGame(skywar.getPlayers());
        
		ChatHandler.broadcastToPlayers(SkyWarsMain.pl, 
				"Last team/man standing wins... Go!", skywar.getPlayers());

        
        skywar.start();

    }

    public boolean stopGame() {
        skywar.stop();

        return false;
    }

    public static Game[] getCurrentGames() {
        return currentGames;
    }

    public void finish() {        
        Player[] temp = skywar.getPlayers();

        for (Player p : skywar.getPlayers()) {
        	if (p == null) continue;
            p.setGameMode(GameMode.SURVIVAL);
            p.teleport(skywar.getArena().getLobby());
            skywar.removePlayer(p);
        }
        
        for (Team t : skywar.getTeams()) {
        	skywar.removeTeam(t);
        }
        
        for (Player p : temp) {
        	skywar.addPlayer(p);
        }
        
        isFinished = false;
        
        skywar.restart();
    }

    public void setFinished() {
        isFinished = true;
    }

    public boolean isFinished() {
        return isFinished;
    }

    public Player[] getStillAlive() {
        Player[] players;
        int count = 0;
        for (int i = 0; i < stillAlive.length; i++) {
            if (stillAlive[i] != null)
                count++;
        }
        players = new Player[count];

        for (int i = 0; i < stillAlive.length; i++) {
            if(stillAlive[i] != null)
                players[i] = stillAlive[i];
        }

        return players;
    }

    @SuppressWarnings("unused")
	public boolean killPlayer(Player p) {
        if (!skywar.containsPlayer(p))
            return false;
        int temp = stillAlive.length;
        for (int i = 0; i < stillAlive.length; i++) {
            if (stillAlive[i].equals(p))
                temp = i;
        }

        for (int i = temp; i < stillAlive.length; i++) {
            if (i + 1 == stillAlive.length) {
                stillAlive[i] = null;
                break;
            }
            stillAlive[i] = stillAlive[i + 1];
            return true;
        }

        return false;
    }

    public Timer getTimer() {
        return timer;
    }

    public static void addToGames(Game g) {
        for (int i = 0; i < currentGames.length; i++) {
            if (currentGames[i] == null) {
                currentGames[i] = g;
                g.getTimer().run();
                return;
            }
            else {
            	if (currentGames[i].getSkywar().getName().equals(g.getSkywar().getName()))
            		return;
            }
        }
        Game[] games = new Game[currentGames.length + 10];
        for (int i = 0; i < currentGames.length; i++) {
            games[i] = currentGames[i];
        }
        currentGames = games;
        addToGames(g);
    }

    private void registerPlayers() {
        SQLManager.registerNewGame(skywar.getPlayers());
    }
    
    public static Game getCurrentGame(String name) {
    	for (Game g : currentGames) {
    		if (g.getSkywar().getArena().getName().equals(name))
    			return g;
    	}
    	
    	return null;
    }

    public static void setup(FileConfiguration f) {
        for (int i = 0; i < currentGames.length; i++) {
        	if (currentGames[i] != null) {
        		currentGames[i].finish();
        		currentGames[i].getTimer().stop();
            	currentGames[i] = null;
        	}
        }

        for (int i = 0; i < f.getKeys(false).size(); i++) {
        	String name = f.getKeys(false).toArray()[i].toString();
            Arena a = new Arena(name, 0, 0, null, null, null, null);
            if (f.getString(name + ".bound1") != null)
            	a.setBound1((Location) f.get(name + ".bound1"));
            if (f.getString(name + ".bound2") != null)
            	a.setBound2((Location) f.get(name + ".bound2"));
            if (f.getString(name + ".lobby") != null)
            	a.setLobby((Location) f.get(name + ".lobby"));
            if (f.getString(name + ".maxplayers") != null)
            	a.setMaxPlayers(f.getInt(name + ".maxplayers"));
            if (f.getString(name + ".minplayers") != null)
            	a.setMinPlayers(f.getInt(name + ".minplayers"));
            if (f.getStringList(name + ".spawns") != null) 
            	if (f.getStringList(name + ".spawns").size() > 0) {
            		Location[] spawns = new Location[f.getStringList(name + ".spawns").size()];
            		for (int c = 0; c < f.getStringList(name + ".spawns").size(); c++) {
            			spawns[c] = a.stringToLoc(f.getStringList(name + ".spawns").toArray()[c].toString());
                	}
            		a.setSpawns(spawns);
            	}
            
            Game g = new Game(new SkyWar(a, GameState.WAITING, new Player[a.getMaxPlayers()]));
            addToGames(g);
        }
    }
}
