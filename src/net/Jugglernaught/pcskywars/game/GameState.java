package net.Jugglernaught.pcskywars.game;

/**
 * Created by Jugglernaught on 10/18/15.
 */
public enum GameState {

    ACTIVE("Active", true), WAITING("Waiting for players", false), STOPPED("Stopped", false),
    	OVERRIDE("Overriden", true);


    private String name;
    private boolean isActive;

    GameState(String name, boolean isActive) {
        this.name = name;
        this.isActive = isActive;
    }

    boolean getIsActive() {
        return isActive;
    }

    String getName() {
        return name;
    }
}
