package net.Jugglernaught.pcskywars.game;

import java.util.ArrayList;

import net.Jugglernaught.pcskywars.arenas.Arena;

import org.bukkit.entity.Player;

/**
 * SkyWars
 *
 * @author Jugglernaught
 *
 * @version 10/18/15
 */

public class SkyWar {
    private String name;
    private Arena arena;
    private GameState gameState;
    private Player[] players;
    private Team[] teams;

    public SkyWar(Arena arena, GameState gameState, Player[] players) {
        this.name = arena.getName();
        this.arena = arena;
        this.gameState = gameState;
        this.players = players;
    }

    public String getName() {
        return name;
    }

    public Arena getArena() {
        return arena;
    }

    public GameState getGameState() {
        return gameState;
    }

    public Player[] getPlayers() {
    	int count = 0;
    	
    	for (int i = 0; i < this.players.length; i++) {
    		if (this.players[i] != null)
    			count++;
    	}
    	
    	if (count == 0) return new Player[0];
    	
    	Player[] p = new Player[count];
    	
    	for (int i = 0; i < p.length; i++) {
    		if (players[i] != null)
    			p[i] = players[i];
    	}
    	
        return p;
    }

    public void setArena(Arena arena) {
        this.arena = arena;
    }

    @SuppressWarnings("unused")
	public void restart() {
        gameState = GameState.WAITING;
        
        for (Team team : teams) {
        	team = null;
        }
    }

    public void start() {
        gameState = GameState.ACTIVE;
    }
    
    public void override() {
    	gameState = GameState.OVERRIDE;
    }

    public void stop() {
        gameState = GameState.STOPPED;
    }

    public void setPlayers(Player[] players) {
        this.players = players;
    }
    
    public boolean addPlayer(Player player) {
        for (int i = 0; i < players.length; i++) {
            if (players[i] == null) {
                players[i] = player;
                if (arena.getLobby() != null)
                	player.teleport(arena.getLobby());
                return true;
            }
        }

        players = resizePlayers();

        addPlayer(player);

        return false;
    }

    public boolean removePlayer(Player player) {
        int temp = -1;
        for (int i = 0; i < players.length; i++) {
        	if (players[i] == null) continue;
            if (players[i].equals(player)) {
                players[i] = null;
                temp = i;
            }
        }

        if (temp == -1)
            return false;

        for (int i = temp; i < getPlayers().length; i++) {
            if (i + 1 == players.length)
                players[i + 1] = null;
            players[i] = players[i + 1];
        }

        return true;
    }

    public boolean containsPlayer(Player player) {
        for (int i = 0; i < players.length; i++) {
        	if (players[i] == null) continue;
            if (players[i].equals(player))
                return true;
        }

        return false;
    }

    public Team[] getTeams() {
    	if (teams == null)
    		teams = new Team[8];
        return teams;
    }

    public ArrayList<TeamColor> getTeamColors() {
        ArrayList<TeamColor> colors = new ArrayList<TeamColor>();
        if (teams == null) teams = new Team[8];
        for (Team t : teams) {
        	if (t == null) continue;
            colors.add(t.getTeamColor());
        }

        return colors;
    }

    public Team getTeam(String color) {
        for (Team t : teams) {
        	if (t == null) continue;
            if (t.getTeamColor().getName().equalsIgnoreCase(color))
                return t;
        }

        return null;
    }

    public Team getTeam(Player player) {
        for (Team t : teams) {
        	if (t == null) continue;
            for (Player p : t.getPlayers()) {
                if (p.equals(player)) return t;
            }
        }

        return null;
    }
    
    public void addTeam(Team t) {
    	if (teams == null)
    		teams = new Team[8];
    	for (int i = 0; i < 8; i++) {
    		if (teams[i] == null) {
    			teams[i] = t;
    		}
    	}
    }
    
    public void removeTeam(Team t) {
    	int count = 8;
    	if (teams == null)
    		teams = new Team[8];
    	for (Team team : teams) {
    		if (team == null) continue;
    		if (team.getTeamColor().equals(t.getTeamColor()))
    			team = null;
    		else count++;
    	}
    	
    	for (int i = count; i < 7; i++) {
    		teams[i] = teams[i + 1];
    	}
    }
    
    public void updateTeam(Team t) {
    	for (int i = 0; i < 8; i++) {
    		if (teams[i] != null && teams[i].getTeamColor().equals(t.getTeamColor())) {
    			for (int c = 0; c < t.getPlayers().length; c++) {
    				if (t.getPlayers()[c] == null) continue;
    				teams[i].addPlayer(t.getPlayers()[c]);
    			}
    		}
    	}
    	
    	addTeam(t);
    }


    private Player[] resizePlayers() {
        Player[] newPlayers = new Player[2 + players.length];

        for (int i = 0; i < players.length; i++) {
            newPlayers[i] = players[i];
        }

        return newPlayers;
    }
}
