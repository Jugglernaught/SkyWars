package net.Jugglernaught.pcskywars.game;

import org.bukkit.entity.Player;

/**
 * Team
 *
 * @author Jugglernaught
 *
 * @version 10/18/15
 */

public class Team {
    private Player[] players;
    private TeamColor teamColor;

    public Team(Player[] players) {
        this.players = players;
    }

    public Team(Player[] players, TeamColor teamColor) {
        this(players);
        this.teamColor = teamColor;
    }

    public Player[] getPlayers() {
        return players;
    }

    public boolean containsPlayer(Player player) {
        for (Player p : getPlayers()) {
            if (p.equals(player))
                return true;
        }
        return false;
    }

    public void addPlayer(Player p) {
    	for (int i = 0; i < players.length; i++) {
    		if (players[i] == null) {
    			players[i] = p;
    			return;
    		}
    	}
    	
    	Player[] members = new Player[players.length + 1];
    	for (int i = 0; i < players.length; i++) {
    		members[i] = players[i];
    	}
    	players = members;
    	
    	addPlayer(p);
    }
    
    public TeamColor getTeamColor() {
        return teamColor;
    }

    public void setTeamColor(TeamColor color) {
        this.teamColor = color;
    }
}
