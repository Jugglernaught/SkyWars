package net.Jugglernaught.pcskywars.ingame;

import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

public class ItemClear {
	
	public static void clearItems(Player p, double radius) {
		for (Entity ent : p.getNearbyEntities(radius, radius, radius)) {
			if (ent.getType().equals(EntityType.DROPPED_ITEM))
				ent.remove();
		}
	}
}
