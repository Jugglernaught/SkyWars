package net.Jugglernaught.pcskywars.ingame;

import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Chest;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 * SupplyDrop
 *
 * @author Jugglernaught
 *
 * @version 10/21/15
 */

public class SupplyDrop {
    private ItemStack[] items;
    private Inventory inv;

    public SupplyDrop(ItemStack[] items) {
        this.items = items;
        inv = Bukkit.getServer().createInventory(null, 27);
    }

    public ItemStack[] getInventory() {
        return items;
    }

    public void drop(Location l) {
        Random r = new Random();
        if (items == null) return;
        for (int i = 0; i < items.length; i++) {
            if (r.nextInt(i + 1) == r.nextInt(i + 1)) {
                int place = r.nextInt(27);
                inv.getContents()[place] = items[i];
            }
        }

        l.getBlock().setType(Material.CHEST);
        Chest chest = (Chest)l.getBlock();
        chest.getInventory().setContents(inv.getContents());
        l.getWorld().playSound(l, Sound.CHICKEN_EGG_POP, 100F, 100F);
    }
}
