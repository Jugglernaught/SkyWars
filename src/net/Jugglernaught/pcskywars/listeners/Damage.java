package net.Jugglernaught.pcskywars.listeners;

import java.util.HashMap;

import net.Jugglernaught.pcskywars.SkyWarsMain;
import net.Jugglernaught.pcskywars.Settings.ChatHandler;
import net.Jugglernaught.pcskywars.Settings.SQLManager;
import net.Jugglernaught.pcskywars.game.Game;
import net.Jugglernaught.pcskywars.game.Team;
import net.Jugglernaught.pcskywars.game.WinDetection;
import net.Jugglernaught.pcskywars.ingame.ItemClear;

import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Damage
 *
 * @author Jugglernaught
 *
 * @version 10/19/15
 */

public class Damage implements Listener {

    private SkyWarsMain pl;
    private HashMap<Player, Player> damageTags = new HashMap<Player, Player>();
    private Game game;

    @EventHandler
    public void onLethalDamage(EntityDamageEvent e) {
    	pl = SkyWarsMain.pl;
    	damageTags = Death.getPlayerDamageTags();
    	
    	if (!e.getEntity().getType().equals(EntityType.PLAYER))
    		return;
    	
    	Player player = (Player)e.getEntity();
    	
    	for (Game g : Game.getCurrentGames()) {
    		if (g == null) continue;
    		if (!g.getSkywar().containsPlayer(player)) {
    			return;
    		}
    	}
    	    	
    	if (player.getHealth() - e.getDamage() <= 0.0) {
    		e.setCancelled(true);
    		for (Game g : Game.getCurrentGames()) {
    			if (g == null) continue;
                for (Team t : g.getSkywar().getTeams()) {
                	if (t == null) continue;
                    if (t.containsPlayer(player)) {
                    	ChatHandler.sendMiniMessage(pl, player.getDisplayName() + " has died to " +
                        	e.getCause().toString().toLowerCase(), g.getSkywar().getPlayers());
                        }
                    game = g;
                    }
                }
            }
    	else return;

    	player.setFlying(true);
    	
    	for (Player p : game.getSkywar().getPlayers()) {
    		p.hidePlayer(player);
    	}
    	
        player.setHealth(20.0);
        player.setExhaustion(0F);
        player.setFoodLevel(20);

        for (ItemStack i : player.getInventory().getContents()) {
        	if (i == null || i.getType().equals(Material.AIR)) continue;
            player.getWorld().dropItem(player.getLocation(), i);
        }

        for (ItemStack i : player.getInventory().getArmorContents()) {
          	if (i == null || i.getType().equals(Material.AIR)) continue;
            player.getWorld().dropItem(player.getLocation(), i);
        }

        for (int i = 0; i < 50; i++) {
            player.getWorld().playEffect(player.getLocation(), Effect.SMOKE, 1, 2);
        }
        player.getWorld().playSound(player.getLocation(), Sound.ENDERMAN_DEATH, 100F, 100F);

        player.getInventory().clear();

        game.killPlayer(player);

        if (game.getStillAlive().length <= 1) {
            ItemClear.clearItems(player, 50.0);
            if (game.getStillAlive().length == 0) {
            	game.setFinished();
            	return; 
            }
            WinDetection.won(game.getStillAlive()[0], game);
        }

        if (game.getStillAlive().length == 2) {
            Team temp = game.getSkywar().getTeam(game.getStillAlive()[0]);
            if (temp != null) {
                WinDetection.won(game.getSkywar().getTeam(game.getStillAlive()[0]), game);
                ItemClear.clearItems(player, 50.0);
            }
        }
    }
    
    @EventHandler
    public void onTakeDamage(EntityDamageByEntityEvent e) {
        pl = SkyWarsMain.pl;
        damageTags = Death.getPlayerDamageTags();

        if (!e.getEntityType().equals(EntityType.PLAYER))
            return;
        if (!e.getDamager().getType().equals(EntityType.PLAYER))
            return;

        final Player player = (Player)e.getEntity();
        final Player damager = (Player)e.getDamager();

        for (Game g : Game.getCurrentGames()) {
            for (Team t : g.getSkywar().getTeams()) {
                if (!t.containsPlayer(player) || !t.containsPlayer(damager))
                    return;
            }
        }
        
        if (player.getHealth() - e.getDamage() > 0)
            return;
        
        e.setCancelled(true);

        Death.getPlayerDamageTags().put(player, damager);

        pl.getServer().getScheduler().scheduleSyncDelayedTask(pl, new Runnable() {
            public void run() {
                Death.getPlayerDamageTags().put(player, null);
            }
        }, 160L);

        if (player.getHealth() - e.getDamage() > 0)
            return;

        e.setCancelled(true);

        boolean doBreak = false;
        
        for (Game g : Game.getCurrentGames()) {
            for (Team t : g.getSkywar().getTeams()) {
                if (t.containsPlayer(player)) {
                    if (damageTags.containsKey(e.getEntity()) && damageTags.get(e.getEntity()) != null) {
                        ChatHandler.broadcastToPlayers(pl, player.getDisplayName() + " has died to " +
                                damageTags.get(e.getEntity()).getDisplayName() + "!", g.getSkywar().getPlayers());
                        SQLManager.registerPlayerKill(damager);
                        doBreak = true;
                        break;
                    }
                game = g;
                }
            }
            if (doBreak) break;
        }

        player.setFlying(true);
    	
    	for (Player p : game.getSkywar().getPlayers()) {
    		p.hidePlayer(player);
    	}

        for (ItemStack i : player.getInventory().getContents()) {
        	if (i == null || i.getType().equals(Material.AIR)) continue;
            player.getWorld().dropItem(player.getLocation(), i);
        }

        for (ItemStack i : player.getInventory().getArmorContents()) {
        	if (i == null || i.getType().equals(Material.AIR)) continue;
            player.getWorld().dropItem(player.getLocation(), i);
        }

        for (int i = 0; i < 50; i++) {
            player.getWorld().playEffect(player.getLocation(), Effect.SMOKE, 1, 2);
        }
        player.getWorld().playSound(player.getLocation(), Sound.ENDERMAN_DEATH, 100F, 100F);

        player.getInventory().clear();

        game.killPlayer(player);

        if (game.getStillAlive().length <= 1) {
        	ItemClear.clearItems(player, 50.0);
        	if (game.getStillAlive().length == 0) {
        		game.setFinished();
        		return;
        	}
            WinDetection.won(game.getStillAlive()[0], game);
        }

        if (game.getStillAlive().length == 2) {
            Team temp = game.getSkywar().getTeam(game.getStillAlive()[0]);
            if (temp != null) {
                WinDetection.won(game.getSkywar().getTeam(game.getStillAlive()[0]), game);
                ItemClear.clearItems(player, 50.0);
            }
        }
    }

}
