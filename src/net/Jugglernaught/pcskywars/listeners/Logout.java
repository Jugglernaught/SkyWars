package net.Jugglernaught.pcskywars.listeners;

import net.Jugglernaught.pcskywars.game.Game;
import net.Jugglernaught.pcskywars.game.GameState;

import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerQuitEvent;

public class Logout {

	@EventHandler
	public void onLogout(PlayerQuitEvent e) {
		for (Game g : Game.getCurrentGames()) {
			if (g == null) return;
			if (g.getSkywar().containsPlayer(e.getPlayer())) {
				if (g.getSkywar().getGameState().equals(GameState.ACTIVE))
					g.killPlayer(e.getPlayer());
				g.getSkywar().removePlayer(e.getPlayer());
			}
		}
	}
}
