package net.Jugglernaught.pcskywars.threads;

import net.Jugglernaught.pcskywars.SkyWarsMain;
import net.Jugglernaught.pcskywars.Settings.ChatHandler;
import net.Jugglernaught.pcskywars.game.Game;
import net.Jugglernaught.pcskywars.game.GameState;
import net.Jugglernaught.pcskywars.game.SkyWar;
import net.Jugglernaught.pcskywars.ingame.SupplyDrop;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

/**
 * Timer
 *
 * @author Jugglernaught
 *
 * @version 10/18/15
 */

public class Timer {
    private int id;
    private int length;
    private Game game;
    private int count;

    private SkyWarsMain pl;

    public Timer(int length, Game game, SkyWarsMain pl) {
        this.length = length;
        this.game = game;
        this.count = length;
        this.id = -1;
        this.pl = pl;
    }

    public Game getGame() {
        return game;
    }

    public int run() {
        final SkyWar sw = this.game.getSkywar();

        id = pl.getServer().getScheduler().scheduleSyncRepeatingTask(pl, new Runnable() {
            public void run() {
                if (sw.getGameState().equals(GameState.WAITING)) {
                    if (sw.getPlayers().length >= sw.getArena().getMinPlayers()) {
                        if (count == 0) {
                            game.startGame();
                            count = pl.getConfig().getInt("time-until-supply-drop");
                            return;
                        }
                        else if (count < 5 || count % 5 == 0) {
                    		ChatHandler.broadcastToPlayers(pl, ChatColor.BOLD  + 
                    				pl.getConfig().getString("timer-message") +
                                String.valueOf(count), sw.getPlayers());
                    		count--;
                    	}
                        else {
                        	count--;
                        }
                    }
                }
                else if (sw.getGameState().equals(GameState.ACTIVE)) {
                    if (count == 0) {
                        for (Player p : game.getStillAlive()) {
                        	if (p == null) continue;
                                                                //TODO Get items of Supply Drop from Config
                            SupplyDrop supplyDrop = new SupplyDrop(null);
                            supplyDrop.drop(p.getLocation());
 //                           ChatHandler.sendMessage(p, "Your supplies have arrived!");
                            count = pl.getConfig().getInt("time-until-supply-drop");
                        }
                    }
                    else if (count == 30) {
//                		ChatHandler.broadcastToPlayers(pl, ChatColor.BOLD  + "Supply Drop in " +
//                                String.valueOf(count) + " seconds!", sw.getPlayers());
                		count --; 
                	}
                    else {
                    	count--;
                    }

                    if (game.isFinished()) {
                        game.stopGame();
                        count = pl.getConfig().getInt("end-game-waiting-time");
                    }
                }
                else if (sw.getGameState().equals(GameState.STOPPED)) {
                    if (count == 0) {
                        game.finish();
                        count = length;
                        return;
                    }
                    else if (count < 3 || count % 10 == 0) {
                		ChatHandler.broadcastToPlayers(pl, ChatColor.BOLD  + "Returning back to lobby in " +
                            String.valueOf(count), sw.getPlayers());
                		count--;
                    }
                    else {
                    	count--;
                    }
                }
                else if (sw.getGameState().equals(GameState.OVERRIDE)) {
                	if (count == 0) {
                        count = pl.getConfig().getInt("time-until-supply-drop");
                        game.startGame();
                        return;
                    }
                	else if (count < 5 || count % 5 == 0) {
                		ChatHandler.broadcastToPlayers(pl, ChatColor.BOLD  + "Game starts in " +
                            String.valueOf(count), sw.getPlayers());
                		count--;
                	}
                	else {
                		count--;
                	}
                }
            }
        }, 1L, 20L);

        return id;
    }

	public void stop() {
		pl.getServer().getScheduler().cancelTask(id);
	}
}
