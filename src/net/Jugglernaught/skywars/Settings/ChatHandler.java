package net.Jugglernaught.pcskywars.Settings;

import net.Jugglernaught.pcskywars.SkyWarsMain;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

/**
 * ChatHandler
 *
 * @author Jugglernaught
 *
 * @version 10/18/15
 */

public class ChatHandler {
    private static String prefix = ChatColor.AQUA + "[PCSW] " + ChatColor.RESET;

    public static void broadcastToPlayers(SkyWarsMain pl, String message) {
        pl.getServer().broadcastMessage(prefix + message);
    }

    public static void broadcastToPlayers(SkyWarsMain pl, String message, Player[] players) {
        for (Player p : players) {
            p.sendMessage(prefix + message);
        }
    }

    public static void sendMessage(Player p, String message) {
        p.sendMessage(prefix + ChatColor.GREEN + message);
    }
}
