package net.Jugglernaught.pcskywars.Settings;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import net.Jugglernaught.pcskywars.SkyWarsMain;

import org.bukkit.entity.Player;

/**
 * SQLManager
 *
 * @author Jugglernaught
 *
 * @version 10/22/15
 */

public class SQLManager {
    private static SkyWarsMain pl = SkyWarsMain.pl;
    private static final String IP = pl.getConfig().getString("ip");
    private static final String PORT = pl.getConfig().getString("port");
    private static final String DATABASE = pl.getConfig().getString("database-name");
    private static final String DB_NAME = "jdbc:mysql://" + IP + ":" + PORT + "/" + DATABASE;
    private static final String USER = pl.getConfig().getString("username");
    private static final String PASS = pl.getConfig().getString("password");
    private static final String TABLE_NAME = pl.getConfig().getString("name-of-skywars-datatable");
    private static Connection connection;

    public synchronized static void openConnection() {
        try {
        	if (IP == null || IP.isEmpty())
        		return;
            connection = DriverManager.getConnection(DB_NAME, USER, PASS);

        } catch (SQLException e) {
            e.printStackTrace();
            return;
        }
    }

    public synchronized static void closeConnection() {
        try {
        	if (IP == null || IP.isEmpty())
        		return;
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public synchronized static boolean isPlayerRegistered(Player p) {
        try {
        	if (IP == null || IP.isEmpty())
        		return false;
            PreparedStatement sql = connection.prepareStatement("SELECT * FROM `" + TABLE_NAME + "` " +
                    "WHERE player=?;");
            sql.setString(1, p.getUniqueId().toString());
            ResultSet rs = sql.executeQuery();
            boolean containsPlayer = rs.next();

            sql.close();
            rs.close();

            return containsPlayer;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @SuppressWarnings("finally")
	public synchronized static boolean registerNewGame(Player[] players) {
        openConnection();
        int previousGames = 0;
        try {
        	if (IP == null || IP.isEmpty())
        		return false;
            for (Player p : players) {
                if (isPlayerRegistered(p)) {
                    PreparedStatement sql = connection.prepareStatement("SELECT games FROM `" + TABLE_NAME + "` WHERE " +
                            "player=?;");
                    sql.setString(1, p.getUniqueId().toString());
                    ResultSet rs = sql.executeQuery();
                    rs.next();

                    previousGames = rs.getInt("games");

                    PreparedStatement gamesUpdate = connection.prepareStatement("UPDATE `" + TABLE_NAME +
                            "` SET games=? WHERE player=?;");
                    gamesUpdate.setInt(1, previousGames + 1);
                    gamesUpdate.setString(2, p.getUniqueId().toString());
                    gamesUpdate.executeUpdate();

                    gamesUpdate.close();
                    rs.close();
                    sql.close();
                } else {
                    PreparedStatement newPlayer = connection.prepareStatement("INSERT INTO `" + TABLE_NAME +
                        "` values(?, ?, 0, 0, 1);");
                    newPlayer.setString(1, p.getUniqueId().toString());
                    newPlayer.setString(2, p.getName());
                    newPlayer.execute();

                    newPlayer.close();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            closeConnection();
            return true;
        }
    }

    @SuppressWarnings("finally")
	public synchronized static boolean registerPlayerKill(Player p) {
        openConnection();
        int kills = 0;

        try {
        	if (IP == null || IP.isEmpty())
        		return false;
            if (!isPlayerRegistered(p))
                return false;
            PreparedStatement playerKills = connection.prepareStatement("SELECT kills FROM `" + TABLE_NAME +
                "` WHERE player=?;");
            playerKills.setString(1, p.getUniqueId().toString());

            ResultSet rs = playerKills.executeQuery();
            rs.next();
            kills = rs.getInt("kills");

            PreparedStatement killUpdate = connection.prepareStatement("UPDATE `" + TABLE_NAME + "` SET kills=?" +
                " WHERE player=?;");
            killUpdate.setInt(1, kills + 1);
            killUpdate.setString(2, p.getUniqueId().toString());
            killUpdate.executeUpdate();

            killUpdate.close();
            rs.close();
            playerKills.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            closeConnection();
            return true;
        }
    }

    @SuppressWarnings("finally")
	public synchronized static boolean registerPlayerWins(Player p) {
        openConnection();
        int wins = 0;
        try {
        	if (IP == null || IP.isEmpty())
        		return false;
            if (!isPlayerRegistered(p))
                return false;
            PreparedStatement sql = connection.prepareStatement("SELECT wins FROM `" + TABLE_NAME + "` WHERE " +
                    "player=?;");
            sql.setString(1, p.getUniqueId().toString());

            ResultSet rs = sql.executeQuery();
            rs.next();
            wins = rs.getInt("wins");

            PreparedStatement winsUpdate = connection.prepareStatement("UPDATE `" + TABLE_NAME + "` SET wins=?" +
                    " WHERE players=?;");
            winsUpdate.setInt(1, wins + 1);
            winsUpdate.setString(2, p.getUniqueId().toString());
            winsUpdate.executeUpdate();

            winsUpdate.close();
            rs.close();
            sql.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            closeConnection();
            return true;
        }
    }
}
