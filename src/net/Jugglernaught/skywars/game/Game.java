package net.Jugglernaught.pcskywars.game;

import java.util.Random;

import net.Jugglernaught.pcskywars.SkyWarsMain;
import net.Jugglernaught.pcskywars.Settings.ChatHandler;
import net.Jugglernaught.pcskywars.Settings.SQLManager;
import net.Jugglernaught.pcskywars.arenas.Arena;
import net.Jugglernaught.pcskywars.threads.Timer;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

/**
 * Game
 *
 * @author Jugglernaught
 *
 * @version 10/18/15
 */

public class Game {
    private SkyWar skywar;
    private static Game[] currentGames = new Game[10];
    private boolean isFinished;
    private Player[] stillAlive;
    private Timer timer;

    public Game(SkyWar skywar) {
        this.skywar = skywar;
        isFinished = false;
        stillAlive = skywar.getPlayers();
        timer = new Timer(SkyWarsMain.pl.getConfig().getInt("time-before-game-starts"), this, SkyWarsMain.pl);
        addToGames(this);
    }

    public SkyWar getSkywar() {
        return skywar;
    }

    public void startGame() {
        skywar.start();

        registerPlayers();

        for (int i = 0; i < skywar.getPlayers().length; i++) {
            if (skywar.getPlayers().length <= 2) {
                TeamColor color;
                Random r = new Random();
                do {
                    color = TeamColor.values()[r.nextInt(2)];
                } while (skywar.getTeamColors().contains(color));

                Player[] p = {skywar.getPlayers()[i]};
                Team t = new Team(p, color);

                ChatHandler.broadcastToPlayers(SkyWarsMain.pl, t.getTeamColor().getChatColor() + "You are now " +
                        t.getTeamColor().getName() + " team!", p);
            }
            TeamColor color;
            Random r = new Random();
            int half = (skywar.getPlayers().length / 2) + (skywar.getPlayers().length % 2);
            do {
                color = TeamColor.values()[r.nextInt(half)];
            } while (skywar.getTeamColors().contains(color));

            if (skywar.getPlayers().length >= i + 1) {
                Player[] p = {skywar.getPlayers()[i], skywar.getPlayers()[i + 1]};
                Team t = new Team(p, color);
                i++;
                ChatHandler.broadcastToPlayers(SkyWarsMain.pl, t.getTeamColor().getChatColor() + "You are now " +
                        t.getTeamColor().getName() + " team!", p);
                continue;
            }
            Player[] p = {skywar.getPlayers()[i]};
            Team t = new Team(p, color);
            i++;

            ChatHandler.broadcastToPlayers(SkyWarsMain.pl, t.getTeamColor().getChatColor() + "You are now " +
                    t.getTeamColor().getName() + " team!", p);
        }

        Location[] spawns = skywar.getArena().getSpawns();

        for (int i = 0; i < skywar.getPlayers().length; i++) {
            int spawnNumber = spawns.length % i;

            for (Player p : skywar.getTeams()[i].getPlayers()) {
                p.setGameMode(GameMode.SURVIVAL);
                p.teleport(skywar.getArena().getSpawns()[spawnNumber]);
            }
        }

        SQLManager.registerNewGame(skywar.getPlayers());

    }

    public boolean stopGame() {
        skywar.stop();

        return false;
    }

    public static Game[] getCurrentGames() {
        return currentGames;
    }

    public boolean finish() {
        skywar.restart();

        for (Player p : skywar.getPlayers()) {
        	if (p == null) continue;
            p.setGameMode(GameMode.SURVIVAL);
            p.teleport(skywar.getArena().getLobby());
        }

        return false;
    }

    public void setFinished() {
        isFinished = true;
    }

    public boolean isFinished() {
        return isFinished;
    }

    public Player[] getStillAlive() {
        Player[] players;
        int count = 0;
        for (int i = 0; i < stillAlive.length; i++) {
            if (stillAlive[i] != null)
                count++;
        }
        players = new Player[count];

        for (int i = 0; i < stillAlive.length; i++) {
            if(stillAlive[i] != null)
                players[i] = stillAlive[i];
        }

        return players;
    }

    @SuppressWarnings("unused")
	public boolean killPlayer(Player p) {
        if (!skywar.containsPlayer(p))
            return false;
        int temp = stillAlive.length;
        for (int i = 0; i < stillAlive.length; i++) {
            if (stillAlive[i].equals(p))
                temp = i;
        }

        for (int i = temp; i < stillAlive.length; i++) {
            if (i + 1 == stillAlive.length) {
                stillAlive[i] = null;
                break;
            }
            stillAlive[i] = stillAlive[i + 1];
            return true;
        }

        return false;
    }

    public Timer getTimer() {
        return timer;
    }

    public static void addToGames(Game g) {
        for (int i = 0; i < currentGames.length; i++) {
            if (currentGames[i] == null) {
                currentGames[i] = g;
                g.getTimer().run();
                return;
            }
        }
        Game[] games = new Game[currentGames.length + 10];
        for (int i = 0; i < currentGames.length; i++) {
            games[i] = currentGames[i];
        }
        currentGames = games;
        addToGames(g);
    }

    private void registerPlayers() {
        SQLManager.registerNewGame(skywar.getPlayers());
    }
    
    public static Game getCurrentGame(String name) {
    	for (Game g : currentGames) {
    		if (g.getSkywar().getArena().getName().equals(name))
    			return g;
    	}
    	
    	return null;
    }

    public static void setup(FileConfiguration f) {
        for (int i = 0; i < currentGames.length; i++) {
        	if (currentGames[i] != null) {
        		currentGames[i].finish();
        		currentGames[i].getTimer().stop();
            	currentGames[i] = null;
        	}
        }

        for (String name : f.getKeys(true)) {
            Arena a = new Arena(name, 0, 0, null, null, null, null);
            if ((Location) f.get(name + ".bound1") != null)
            	a.setBound1((Location) f.get(name + ".bound1"));
            if ((Location) f.get(name + ".bound2") != null)
            	a.setBound2((Location) f.get(name + ".bound2"));
            if ((Location) f.get(name + ".lobby") != null)
            	a.setLobby((Location) f.get(name + ".lobby"));
            if (f.getString(name + ".maxplayers") != null)
            	a.setMaxPlayers(f.getInt(name + "maxplayers"));
            if (f.getString(name + ".minplayers") != null)
            	a.setMinPlayers(f.getInt(name + "minplayers"));
            if (f.getStringList(name + ".spawns") != null) 
            	if (f.getStringList(name + ".spawns").size() > 0) {
            		Location[] spawns = new Location[f.getStringList(name + ".spawns").size()];
            		for (int i = 0; i < f.getStringList(name + ".spawns").size(); i++) {
            			spawns[i] = a.stringToLoc(f.getStringList(name + ".spawns").toArray()[i].toString());
                	}
            		a.setSpawns(spawns);
            	}

            Game g = new Game(new SkyWar(a, GameState.WAITING, new Player[a.getMaxPlayers()]));
            addToGames(g);
        }
    }
}
