package net.Jugglernaught.pcskywars.game;

import java.util.ArrayList;

import net.Jugglernaught.pcskywars.arenas.Arena;

import org.bukkit.entity.Player;

/**
 * SkyWars
 *
 * @author Jugglernaught
 *
 * @version 10/18/15
 */

public class SkyWar {
    private String name;
    private Arena arena;
    private GameState gameState;
    private Player[] players;
    private Team[] teams;

    public SkyWar(Arena arena, GameState gameState, Player[] players) {
        this.name = arena.getName();
        this.arena = arena;
        this.gameState = gameState;
        this.players = players;
    }

    public String getName() {
        return name;
    }

    public Arena getArena() {
        return arena;
    }

    public GameState getGameState() {
        return gameState;
    }

    public Player[] getPlayers() {
        return players;
    }

    public void setArena(Arena arena) {
        this.arena = arena;
    }

    public void restart() {
        gameState = GameState.WAITING;
    }

    public void start() {
        gameState = GameState.ACTIVE;
    }
    
    public void override() {
    	gameState = GameState.OVERRIDE;
    }

    public void stop() {
        gameState = GameState.STOPPED;
    }

    public void setPlayers(Player[] players) {
        this.players = players;
    }
    
    public boolean addPlayer(Player player) {
        for (int i = 0; i < players.length; i++) {
            if (players[i] == null) {
                players[i] = player;
                if (arena.getLobby() != null)
                	player.teleport(arena.getLobby());
                return true;
            }
        }

        players = resizePlayers();

        addPlayer(player);

        return false;
    }

    public boolean removePlayer(Player player) {
        int temp = -1;
        for (int i = 0; i < players.length; i++) {
        	if (players[i] == null) continue;
            if (players[i].equals(player)) {
                players[i] = null;
                temp = i;
            }
        }

        if (temp == -1)
            return false;

        for (int i = temp; i < players.length; i++) {
            if (i + 1 >= players.length)
                break;
            players[i] = players[i + 1];
        }

        return true;
    }

    public boolean containsPlayer(Player player) {
        for (int i = 0; i < players.length; i++) {
        	if (players[i] == null) continue;
            if (players[i].equals(player))
                return true;
        }

        return false;
    }

    public Team[] getTeams() {
        return teams;
    }

    public ArrayList<TeamColor> getTeamColors() {
        ArrayList<TeamColor> colors = new ArrayList<TeamColor>();
        for (Team t : teams) {
            colors.add(t.getTeamColor());
        }

        return colors;
    }

    public Team getTeam(String color) {
        for (Team t : teams) {
            if (t.getTeamColor().getName().equalsIgnoreCase(color))
                return t;
        }

        return null;
    }

    public Team getTeam(Player player) {
        for (Team t : teams) {
            for (Player p : t.getPlayers()) {
                if (p.equals(player)) return t;
            }
        }

        return null;
    }


    private Player[] resizePlayers() {
        Player[] newPlayers = new Player[2 + players.length];

        for (int i = 0; i < players.length; i++) {
            newPlayers[i] = players[i];
        }

        return newPlayers;
    }
}
