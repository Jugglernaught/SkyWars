package net.Jugglernaught.pcskywars.game;

import org.bukkit.entity.Player;

/**
 * Team
 *
 * @author Jugglernaught
 *
 * @version 10/18/15
 */

public class Team {
    private Player[] players;
    private TeamColor teamColor;

    public Team(Player[] players) {
        this.players = players;
    }

    public Team(Player[] players, TeamColor teamColor) {
        this(players);
        this.teamColor = teamColor;
    }

    public Player[] getPlayers() {
        return players;
    }

    public boolean containsPlayer(Player player) {
        for (Player p : getPlayers()) {
            if (p.equals(player))
                return true;
        }
        return false;
    }

    public TeamColor getTeamColor() {
        return teamColor;
    }

    public void setTeamColor(TeamColor color) {
        this.teamColor = color;
    }
}
