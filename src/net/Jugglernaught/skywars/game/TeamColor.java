package net.Jugglernaught.pcskywars.game;

import org.bukkit.ChatColor;

/**
 * Created by Jugglernaught on 10/18/15.
 */
public enum TeamColor {

    BLUE("Blue", ChatColor.BLUE), GREEN("Green", ChatColor.DARK_GREEN), RED("Red", ChatColor.DARK_RED),
    AQUA("Aqua", ChatColor.DARK_AQUA), PURPLE("Purple", ChatColor.DARK_PURPLE), GOLD("Gold", ChatColor.GOLD),
    WHITE("White", ChatColor.WHITE), BLACK("Black", ChatColor.BLACK);

    private String name;
    private ChatColor color;

    TeamColor(String name, ChatColor color) {
        this.name = name;
        this.color = color;
    }

    String getName() {
        return name;
    }

    ChatColor getChatColor() {
        return color;
    }
}
