package net.Jugglernaught.pcskywars.game;

import net.Jugglernaught.pcskywars.SkyWarsMain;
import net.Jugglernaught.pcskywars.Settings.ChatHandler;
import net.Jugglernaught.pcskywars.Settings.SQLManager;

import org.bukkit.entity.Player;

/**
 * WinDetection
 *
 * @author Jugglernaught
 *
 * @version 10/20/15
 */

public class WinDetection {

    //TODO Titles + subtitles

    public static boolean won(Player p, Game g) {
        ChatHandler.broadcastToPlayers(SkyWarsMain.pl, p.getDisplayName() + " has won!", g.getSkywar().getPlayers());
        SQLManager.registerPlayerWins(p);
        g.stopGame();
        return false;
    }

    public static boolean won(Team t, Game g) {
        ChatHandler.broadcastToPlayers(SkyWarsMain.pl, t.getTeamColor().getName() + " Team has won!",
                g.getSkywar().getPlayers());
        SQLManager.registerPlayerWins(t.getPlayers()[0]);
        SQLManager.registerPlayerWins(t.getPlayers()[1]);
        g.stopGame();
        return false;
    }
}
