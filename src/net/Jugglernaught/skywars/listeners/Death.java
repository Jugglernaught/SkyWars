package net.Jugglernaught.pcskywars.listeners;

import java.util.HashMap;

import net.Jugglernaught.pcskywars.game.Game;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

/**
 * Death
 *
 * @author Jugglernaught
 *
 * @version 10/19/15
 */

public class Death implements Listener {
    private static HashMap<Player, Player> damageTags = new HashMap<Player, Player>();

    @EventHandler
    public void onPlayerDeathEvent(PlayerDeathEvent e) {
        Player player = e.getEntity();
        for (Game g : Game.getCurrentGames()) {
            if (g.getSkywar().containsPlayer(player)) {
                g.killPlayer(player);
            }
        }
    }

    public static HashMap<Player, Player> getPlayerDamageTags() {
        return damageTags;
    }
}
